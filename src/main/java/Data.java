import org.json.JSONArray;
import org.json.JSONObject;


import java.sql.*;
import java.util.Objects;
import java.util.StringJoiner;

public class Data {
    //MYSQL Connection
    private final Connection MYSQLCONNECT = new MySQLConnect().connect();

    //Most used queries
    private final String SELECTALL = "SELECT * FROM ";
    private final String SELECTLASTINSERDID = "SELECT LAST_INSERT_ID() FROM ";

    //Data from the tables that don't change
    private final JSONArray workoutType;
    private final JSONArray professionType;
    private final JSONArray workoutGoal;
    private final JSONArray workoutLevel;
    private final JSONArray workoutFocus;
    private final JSONArray exercises;
    private final JSONArray setType;
    private final JSONArray setProcess;
    private final JSONArray workoutInformation;
    private final JSONArray userRole;
    private final JSONArray companyDepartment;
    private final JSONArray industry;

    public Data() {
        System.out.println(getSpecificRow("Company", 1));
        this.companyDepartment = getAllRows("Company_department");
        this.workoutType = getAllRows("Workout_type");
        this.professionType = getAllRows("Profession_type");
        this.workoutGoal = getAllRows("Workout_goal");
        this.workoutLevel = getAllRows("Workout_level");
        this.workoutFocus = getAllRows("Workout_focus");
        this.exercises = getAllRows("Exercises");
        this.setType = getAllRows("Set_type");
        this.setProcess = getAllRows("Set_process");
        this.workoutInformation = getAllRows("Admin_workout_information");
        this.userRole = getAllRows("UserRoles");
        this.industry = getAllRows("Industry");
    }

    //A function to get all data from a table
    public JSONArray getAllRows(String table) {
        try {
            //Makes a connection and runs a query to fetch all rows. The query is SELECT * FROM `table`
            PreparedStatement stmt = MYSQLCONNECT.prepareStatement(SELECTALL + table);

            //Get the amount of columns
            int columns = stmt.getMetaData().getColumnCount();

            //Executes the query
            ResultSet rs = stmt.executeQuery();

            //New JSON array
            JSONArray array = new JSONArray();

            //While there are more rows, fill the JSONArray with objects that contain data from rows.
            while (rs.next()) {

                //New JSONobject
                JSONObject obj = new JSONObject();

                //Go past every column to put in the column name and the data of the row in JSONObject.
                for (int i = 1; i <= columns; i++) {
                    //Get the Column name
                    String columnName = stmt.getMetaData().getColumnName(i);

                    //Get the matching object
                    Object object = rs.getObject(i);

                    //Put it in the JSONobject
                    //Objects.requireNonNullElse means that if the object is 'Java' null, than the JSONObject will also be a 'JSON' null.
                    obj.put(columnName, Objects.requireNonNullElse(object, JSONObject.NULL));
                }
                //Put it in the array
                array.put(obj);
            }
            return array;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONArray getSpecificRows(String table, int id) {
        try {
            //A query that selects all columns.
            ResultSetMetaData columns = MYSQLCONNECT.prepareStatement(SELECTALL + table).getMetaData();

            //Select all data of a row if the first colum of the ID is the same as the ID data you put in.
            PreparedStatement stmt = MYSQLCONNECT.prepareStatement(SELECTALL + table + " WHERE " + columns.getColumnName(1) + "=" + id);

            //Get the amount of columns
            int columnCount = stmt.getMetaData().getColumnCount();

            //Execute the query
            ResultSet rs = stmt.executeQuery();

            //New JSON array
            JSONArray array = new JSONArray();

            //While there are rows left fill in the JSONArray with objects that contains data out of a row.
            while (rs.next()) {

                //New JSONobject
                JSONObject obj = new JSONObject();

                //Go past every column to put in the column name and data of the row in the JSONOBject.
                for (int i = 1; i <= columnCount; i++) {
                    //Get the Column name
                    String columnName = stmt.getMetaData().getColumnName(i);

                    //Get the matching object
                    Object object = rs.getObject(i);

                    //Put it in a JSONobject
                    //Objects.requireNonNullElse Means that if the object is 'Java' null, than the JSON object will also be 'JSON' null.
                    obj.put(columnName, Objects.requireNonNullElse(object, JSONObject.NULL));
                }
                //Put the object in an array
                array.put(obj);
            }
            return array;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject getSpecificRow(String table, int id) {
        try {
            //A query that selects all columns
            ResultSetMetaData columns = MYSQLCONNECT.prepareStatement(SELECTALL + table).getMetaData();

            //Select all data of a row if the first columns id equals the id that you put in.
            PreparedStatement stmt = MYSQLCONNECT.prepareStatement(SELECTALL + table + " WHERE " + columns.getColumnName(1) + "=" + id);

            //Get the amount of columns
            int columnCount = stmt.getMetaData().getColumnCount();

            //Execute the query
            ResultSet rs = stmt.executeQuery();

            //New JSONobject
            JSONObject obj = new JSONObject();

            while (rs.next()) {

                //Cycle through each column to put the column name and data of the row at the column in the JSONObject.
                for (int i = 1; i <= columnCount; i++) {
                    //get the columnname
                    String columnName = stmt.getMetaData().getColumnName(i);

                    //Get the matching object
                    Object object = rs.getObject(i);

                    //Put it in a JSONObject
                    //Objects.requireNonNullElse means that if the object is a 'Java' null, then it will also be set as a 'JSON' null in the JSON object.
                    obj.put(columnName, Objects.requireNonNullElse(object, JSONObject.NULL));
                }
            }
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer insertRow(String table, JSONObject newRow, int autoIncrementColumn) {
        try {
            //A query to select all columns
            ResultSetMetaData columns = MYSQLCONNECT.prepareStatement(SELECTALL + table).getMetaData();

            //Gets the column amount
            int columnCount = columns.getColumnCount();

            //StringJoiner is an object that separates between every String that is added. It separes with ','.
            StringJoiner allValues = new StringJoiner(",");
            for (int i = 1; i <= columnCount; i++) {
                allValues.add("?");
            }
            //AllValues output is ?,?,?,etc..

            //Query that prepares an insert for a table with a certain number of columns to be filled.
            PreparedStatement stmt = MYSQLCONNECT.prepareStatement("INSERT INTO " + table + " () VALUES (" + allValues + ")");

            //Goes through every column to insert the corresponding data.
            for (int i = 1; i <= columnCount; i++) {

                if (i == autoIncrementColumn) {
                    //De eerste colom is altijd de id dus zet de eerste colom op 0 zodat de autoincrement het id aanpast.
                    stmt.setObject(i, 0);

                    //Breaks this iteration of the for loop.
                    continue;
                }

                //Creates an object based on the JSONObject that is sent along.
                //He takes the column name as key and takes its value.
                Object obj = newRow.get(columns.getColumnName(i));

                //If the object is null set the MySQL NULL in the database.
                if (obj == JSONObject.NULL) {
                    stmt.setNull(i, Types.INTEGER);
                } else {
                    stmt.setObject(i, obj);
                }
            }

            //Runs the query
            stmt.executeUpdate();

            //This selects the last insert from the table on which you inserted last.
            PreparedStatement stmts = MYSQLCONNECT.prepareStatement(SELECTLASTINSERDID + table);
            ResultSet rs = stmts.executeQuery();
            rs.next();

            return rs.getInt(1);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer updateRow(String table, JSONObject newRow, int id, int autoIncrementColumn) {
        try {
            //A query that selects all columns.
            ResultSetMetaData columns = MYSQLCONNECT.prepareStatement(SELECTALL + table).getMetaData();

            //Get the column number
            int columnCount = columns.getColumnCount();

            //StringJoiner is an object that separates between every String that is added. It separes with ','.
            StringJoiner allValues = new StringJoiner(", ");
            for (int i = 2; i <= columnCount; i++) {
                String columnName = columns.getColumnName(i);
                allValues.add(columnName + " = COALESCE(?, " + columnName + ")");
            }
            //Allvalues ouput is 'columname' = ?, 'columname' = ?, 'columname' = ?, etc..

            //Prepares the query
            // = ? is added at the end, because the StringJoiner only separates the Strings. And at the end there shouldn't be a '= ?'.
            PreparedStatement stmt = MYSQLCONNECT.prepareStatement("UPDATE " + table + " SET " + allValues + " " + " WHERE " + columns.getColumnName(autoIncrementColumn) + "=" + id);

            //Goes To every column and fills in the corresponding value
            for (int i = 1; i < columnCount; i++) {
                //Create an object based on the JSONObject that is given.
                //It picks the columname if the key and picks the value thereof
                Object obj = newRow.get(columns.getColumnName(i + 1));

                //If the object is null, then set the MySQL null in the database
                //Java null value is different from MySql null value
                if (obj == JSONObject.NULL) {
                    stmt.setNull(i, Types.INTEGER);
                } else {
                    stmt.setObject(i, obj);
                }
            }
            //Executes the query
            stmt.executeUpdate();

            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer deleteRow(String table, int id) {
        try {
            //A query to select all columns
            ResultSetMetaData columns = MYSQLCONNECT.prepareStatement(SELECTALL + table).getMetaData();

            //Prepares a statement to delete the rows from the table where the first column, the id, is equal to the given id
            PreparedStatement stmt = MYSQLCONNECT.prepareStatement("DELETE FROM " + table + " WHERE " + columns.getColumnName(1) + "=" + id);

            //Executes the query
            return stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public JSONArray getExercises() {
        return exercises;
    }

    public JSONArray getCompanyDepartment() {
        return companyDepartment;
    }

    public JSONArray getIndustry() {
        return industry;
    }

    public JSONArray getProfessionType() {
        return professionType;
    }

    public JSONArray getSetProcess() {
        return setProcess;
    }

    public JSONArray getSetType() {
        return setType;
    }

    public JSONArray getUserRole() {
        return userRole;
    }

    public JSONArray getWorkoutFocus() {
        return workoutFocus;
    }

    public JSONArray getWorkoutGoal() {
        return workoutGoal;
    }

    public JSONArray getWorkoutInformation() {
        return workoutInformation;
    }

    public JSONArray getWorkoutLevel() {
        return workoutLevel;
    }

    public JSONArray getWorkoutType() {
        return workoutType;
    }
}