import org.json.JSONObject;
import spark.Request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static spark.Spark.*;

public class Endpoints {

    private final Data data;

    public Endpoints(Data data) {
        this.data = data;
    }

    //Retrieves all Endpoints for get, post, put, delete.

    /*
    Options: An action that is called to add options for permissions and CORS policies.
    Before: An action that is called before the endpoint is processed

    Get: An action where an Endpoint retrieves data from the database.
    Post: An action where an Endpoint adds a new data to the database.
    Put: An action where an Endpoint data is edited in the database.
    Delete: An action where an Endpoint deletes data in the database.

    After: An action that is called after an Endpoint has been used.
    Notfound: An action that is called if an Endpoint is not found.
     */
    public void getEndpoints() {
        port(getHerokuAssignedPort());

        options("/*",
                (req, res) -> {

                    String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        res.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        res.header("Access-Control-Allow-Methods", accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.type("application/json");
        });

        //https://naredofrontend.vercel.app

        //Localhost:4567/exercises
        get("/exercises", (req, res) -> {
            checkAuthentication(req);
            return data.getExercises();
        });

        //Localhost:4567/professiontype
        get("/professiontype", (req, res) -> {
            checkAuthentication(req);
            return data.getProfessionType();
        });

        //Localhost:4567/setprocess
        get("/setprocess", (req, res) -> {
            checkAuthentication(req);
            return data.getSetProcess();
        });

        //Localhost:4567/settype
        get("/settype", (req, res) -> {
            checkAuthentication(req);
            return data.getSetType();
        });

        //Localhost:4567/workout
        get("/workout", (req, res) -> {
            checkAuthentication(req);
            return data.getAllRows("Workout");
        });

        //Localhost:4567/workoutfocus
        get("/workoutfocus", (req, res) -> {
            checkAuthentication(req);
            return data.getWorkoutFocus();
        });

        //Localhost:4567/workoutgoal
        get("/workoutgoal", (req, res) -> {
            checkAuthentication(req);
            return data.getWorkoutGoal();
        });

        //Localhost:4567/workoutlevel
        get("/workoutlevel", (req, res) -> {
            checkAuthentication(req);
            return data.getWorkoutLevel();
        });

        //Localhost:4567/workouttype
        get("/workouttype", (req, res) -> {
            checkAuthentication(req);
            return data.getWorkoutType();
        });
        //Localhost:4567/user

        get("/account", (req, res) -> {
            checkAuthentication(req);
            return data.getAllRows("Users");
        });

        get("/workoutinformation", (req, res) -> {
            checkAuthentication(req);
            return data.getWorkoutInformation();
        });

        get("/userrole", (req, res) -> {
            checkAuthentication(req);
            return data.getUserRole();
        });

        get("/userrole/:id", (req, res) -> {
            checkAuthentication(req);
            return data.getSpecificRow("UserRoles", Integer.parseInt(req.params(":id")));
        });

        post("/account", ((req, res) -> {
            checkAuthentication(req);
            JSONObject object = new JSONObject(req.body());
            object.put("Password", Login.createPassword(object.get("Password").toString()));

            return data.insertRow("Users", object, 1);
        }));

        put("/account/:id", ((req, res) -> {
            checkAuthentication(req);
            JSONObject object = new JSONObject(req.body());
            object.put("Password", Login.createPassword(object.get("Password").toString()));

            return data.updateRow("Users", object, Integer.parseInt(req.params(":id")), 1);
        }));

        delete("/account/:id", (req, res) -> {
            checkAuthentication(req);
            return data.deleteRow("Users", Integer.parseInt(req.params(":id")));
        });

        get("/onboarding", (req, res) -> {
            checkAuthentication(req);
            return data.getAllRows("Company");
        });

        put("/onboarding/:id", ((req, res) -> {
            checkAuthentication(req);
            int id = Integer.parseInt(req.params(":id"));
            JSONObject onboarding = new JSONObject(req.body());
            data.updateRow("Company", onboarding, id, 1);

            ArrayList<String> updateKeys = getUpdateKeys(onboarding);

            //For every key in the need to be updated keys list
            for (String updateKey : updateKeys) {
                JSONObject department = new JSONObject();
                department.put("FK_Company_id", id);

                //Any new department contains a '?' as the key
                //Any already existing department does not have a '?' in the key
                if (updateKey.contains("?")) {
                    department.put("Department_name", onboarding.get(updateKey));
                    data.insertRow("Company_department", department, 2);
                } else {
                    //Get the id by splitting the key with the '_'
                    //Output will be for example: '[Department, id, 10]'
                    //The id is always the last in the array
                    String[] splitUpdateKey = updateKey.split("_");
                    int currentDepartmentId = Integer.parseInt(splitUpdateKey[splitUpdateKey.length - 1]);

                    //Some values cant be null so will have to fill them with the correct values
                    department.put("Department_name", onboarding.get(updateKey));
                    department.put("Department_id", currentDepartmentId);
                    data.updateRow("Company_department", department, currentDepartmentId, 2);
                }
            }
            return 1;
        }));

        post("/onboarding", ((req, res) -> {
            checkAuthentication(req);

            //First insert the Company
            JSONObject onboarding = new JSONObject(req.body());
            Integer id = data.insertRow("Company", onboarding, 1);

            //Than insert the Departments
            for (int i = 1; i <= onboarding.length() - 4; i++) {
                JSONObject department = new JSONObject();
                department.put("FK_Company_id", id);
                department.put("Department_name", onboarding.get("Department_id_?" + i));
                data.insertRow("Company_department", department, 2);
            }
            return id;
        }));

        delete("/onboarding/:id", (req, res) -> {
            checkAuthentication(req);
            return data.deleteRow("Company", Integer.parseInt(req.params(":id")));
        });

        get("/departments", (req, res) -> {
            checkAuthentication(req);
            return data.getCompanyDepartment();
        });

        get("/department/:id", (req, res) -> {
            checkAuthentication(req);
            return data.getSpecificRows("Company_department", Integer.parseInt(req.params(":id")));
        });

        put("/department/:id", ((req, res) -> {
            checkAuthentication(req);
            res.type("application/json");
            return data.updateRow("Company_department", new JSONObject(req.body()), Integer.parseInt(req.params(":id")), 1);
        }));

        post("/department", ((req, res) -> {
            checkAuthentication(req);
            return data.insertRow("Company_department", new JSONObject(req.body()), 1);
        }));

        delete("/department/:id", (req, res) -> {
            checkAuthentication(req);
            res.type("application/json");
            return data.deleteRow("Company_department", Integer.parseInt(req.params(":id")));
        });

        get("/industry", (req, res) -> {
            checkAuthentication(req);
            return data.getIndustry();
        });

        post("/industry", (req, res) -> {
            checkAuthentication(req);
            return data.insertRow("Industry", new JSONObject(req.body()), 1);
        });

        put("/industry/:id", (req, res) -> {
            checkAuthentication(req);
            res.type("application/json");
            return data.updateRow("Industry", new JSONObject(req.body()), Integer.parseInt(req.params(":id")), 1);
        });

        delete("/industry/:id", (req, res) -> {
            checkAuthentication(req);
            res.type("application/json");
            return data.deleteRow("Industry", Integer.parseInt(req.params(":id")));
        });

        //Localhost:4567/workout
        post("/workout", ((req, res) -> {
            checkAuthentication(req);
            return data.insertRow("Workout", new JSONObject(req.body()), 1);
        }));

        //Localhost:4567/workout/:workoutid
        put("/workout/:id", ((req, res) -> {
            checkAuthentication(req);
            return data.updateRow("Workout", new JSONObject(req.body()), Integer.parseInt(req.params(":id")), 1);
        }));

        //Localhost:4567/login
        post("/login", (req, res) -> {
            JSONObject obj = new JSONObject(req.body());
            Login login = new Login(obj.getString("username"), obj.getString("password"), data);

            if (login.checkUser() == null) {
                return 0;
            }
            return login.checkUser();
        });

        delete("/workout/:id", (req, res) -> {
            checkAuthentication(req);
            return data.deleteRow("Workout", Integer.parseInt(req.params(":id")));
        });

        after((req, res) -> {
            res.status(200);
        });

        notFound((req, res) -> {
            res.status(404);
            return "{\"message\":\"404 Not Found\"}";
        });
    }

    public static int[] getDepartmentCountTest(String onboardingItem, int id) {
        JSONObject onboarding = new JSONObject(onboardingItem);
        ArrayList<String> updateKeys = getUpdateKeys(onboarding);

        int editDepartment = 0;
        int newDepartment = 0;
        //For every key in the need to be updated keys list
        for (String updateKey : updateKeys) {
            JSONObject department = new JSONObject();
            department.put("FK_Company_id", id);

            //Any new department contains a '?' as the key
            //Any already existing department does not have a '?' in the key
            if (updateKey.contains("?")) {
                department.put("Department_name", onboarding.get(updateKey));
                newDepartment += 1;
            } else {
                //Get the id by splitting the key with the '_'
                //Output will be for example: '[Department, id, 10]'
                //The id is always the last in the array
                String[] splitUpdateKey = updateKey.split("_");
                int currentDepartmentId = Integer.parseInt(splitUpdateKey[splitUpdateKey.length - 1]);

                //Some values cant be null so will have to fill them with the correct values
                department.put("Department_name", onboarding.get(updateKey));
                department.put("Department_id", currentDepartmentId);
                editDepartment += 1;
            }
        }

        //Return an int array with 2 values.
        //The first one for the department count that are new, the second for departments that need to be edited
        int[] departmentCount = new int[2];
        departmentCount[0] = newDepartment;
        departmentCount[1] = editDepartment;
        return departmentCount;
    }

    public static ArrayList<String> getUpdateKeys(JSONObject onboarding) {
        ArrayList<String> updateKeys = new ArrayList<>();

        //Check for all keys
        Iterator<String> keys = onboarding.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            //For every department add keys to arraylist
            if (key.contains("Department_id_")) {
                updateKeys.add(key);
            }
        }

        return updateKeys;
    }

    //Gets a Port for a process builder from Heroku.
    static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }

    //Checks if user has been authenticated.
    private void checkAuthentication(Request req) {
        String authentication = req.headers("Authentication");

        if (authentication == null) {
            halt(401, "User Unauthorized");
        }
    }
}
