import java.sql.PreparedStatement;
import java.sql.ResultSet;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.json.JSONObject;

public class Login {
    //Connection with the database
    private final MySQLConnect MYSQLCONNECT = new MySQLConnect();

    //Class attributes
    private final Data DATA;
    private final String USERNAME;
    private final String PASSWORD;

    public Login(String username, String password, Data data) {
        this.USERNAME = username;
        this.PASSWORD = password;
        this.DATA = data;
    }

    //Method to check a user
    public JSONObject checkUser() {
        try {
            //Prepare a query to get all Users
            PreparedStatement stmt = MYSQLCONNECT.connect().prepareStatement("SELECT * FROM Users WHERE Username='" + USERNAME + "'");

            //Execute the query
            ResultSet rs = stmt.executeQuery();

            //While there are next rows, compare the users
            while (rs.next()) {
                //Get the password from the database Users table
                String dbPassword = rs.getString(3);

                //Compare the passwords
                BCrypt.Result result = checkPassword(dbPassword, PASSWORD);

                //If the password and the username is correct
                if (result.verified) {
                    //Returns the Roll of the user
                    JSONObject userData = DATA.getSpecificRow("UserRoles", rs.getInt(4));
                    userData.put("User_id", rs.getInt(1));
                    return userData;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Method to check the password
    public BCrypt.Result checkPassword(String dbPassword, String password) {
        //Compares the filled password with the password from the User table
        return BCrypt.verifyer().verify(password.toCharArray(), dbPassword);
    }

    //Method to hash a password
    public static String createPassword(String password) {
        //The cost calculates how much the password is being hashed, this will be more exponentially
        //A cost of 12 is 2^12, while a cost of 13 is 2^13
        return BCrypt.withDefaults().hashToString(12, password.toCharArray());
    }
}
