public class Main {

    //Start the Endpoint and Data class.
    public static void main(String[] args) {
        //Add a task before the program closes.
        Runtime.getRuntime().addShutdownHook(new ShutDownTask());
        new Endpoints(new Data()).getEndpoints();
    }

    private static class ShutDownTask extends Thread {
        @Override
            public void run() {
            //Before closing the program, disconnect all connections to the database.
            MySQLConnect.disconnect();
        }
    }
}
