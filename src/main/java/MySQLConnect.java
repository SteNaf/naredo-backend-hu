import java.sql.*;
import java.util.Properties;

public class MySQLConnect {
    //Values that is required for the connection to the database.
    private final String DATABASE_URL = System.getenv("DB_URL");
    private final String USERNAME = System.getenv("DB_USERNAME");
    private final String PASSWORD = System.getenv("DB_PASSWORD");

    private static Connection connection;
    private Properties properties;

    //Fills the credentials in the property object.
    public Properties getProperties() {
        //If the properties are empty, fill the properties
        if (properties == null) {
            properties = new Properties();
            //Set the property with the correct key and associated data.
            properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
        }
        return properties;
    }

    public static Connection getConnection() {
        return connection;
    }

    //Connection with the database.
    public Connection connect() {
        //If the connection doesn't exist create a new connection.
        if (connection == null) {
            try {
                //An connection to a database that requires different parameters.
                //In this case it is the URL of the database, with the username and associated password. This is in the getProperties() method.
                connection = DriverManager.getConnection(DATABASE_URL, getProperties());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    //Disconnect the database.
    public static void disconnect() {
        //If the connection still exist, close it.
        if (connection != null) {
            try {
                //Method that closes the connection.
                connection.close();

                //Set the connection to null so that this method cannot run if there is no connection.
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
