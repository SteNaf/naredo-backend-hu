import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DataTest {

    @Test
    void getAllRowsShouldReturnNotNull() {
        Data data = new Data();
        //If this return null the Table 'Workout' does not exist or is empty
        assertNotNull(data.getAllRows("Workout"));
    }

    @Test
    void getExerciseShouldReturnNotNull() {
        Data data = new Data();
        //If this return null the Table 'Exercises' does not exist or is empty
        assertNotNull(data.getExercises());
    }

    @Test
    void getSpecificRowShouldReturnNotNull() {
        Data data = new Data();
        //If this returns true, an invalid Id was passed as the parameter or the table does not exist
        assertFalse(data.getSpecificRow("Workout", 8).isEmpty());
    }

    @Test
    void getSpecificRowsShouldReturnNotNull() {
        Data data = new Data();
        //If this returns true, an invalid Id was passed as the parameter or the table does not exist
        assertFalse(data.getSpecificRows("Company_department", 1).isEmpty());
    }

    @Test
    void deleteItemShouldReturnFalse() {
        Data data = new Data();
        //Because id is an invalid number it should return 0
        assertNotEquals(1, data.deleteRow("Workout", -1));
    }

    @Test
    void updateItemWithNullShouldReturnFalse() {
        Data data = new Data();
        //Because item is null method should return null
        assertNull(data.updateRow("Workout", null, 8, 1));
    }

    @Test
    void updateItemWithWrongObjectShouldReturnFalse() {
        Data data = new Data();
        //Because item is wrong method should return null
        assertNull(data.updateRow("Workout", new JSONObject("{\"foo\":\"bar\"}"), 8, 1));
    }

    @Test
    void addItemShouldReturnFalse() {
        Data data = new Data();
        //Because item is null it should return null
        assertNull(data.insertRow("Workout", null, 1));
    }

    @Test
    void addItemWithWrongObjectShouldReturnFalse() {
        Data data = new Data();
        //Because item is wrong method should return null
        assertNull(data.insertRow("Workout", new JSONObject("{\"foo\":\"bar\"}"), 1));
    }
}