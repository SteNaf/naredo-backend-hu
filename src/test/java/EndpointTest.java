import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

public class EndpointTest {
    @Test
    void getUpdateKeysShouldNotReturnEmpty() {
        JSONObject onboardingItem = new JSONObject("{\"Department_id_7\":\"BlaBlaBla\"}");
        ArrayList<String> departments = Endpoints.getUpdateKeys(onboardingItem);
        //If a deparment is added with the correct format 'Department_id_x' it should be added to a list and thus not return empty
        assertFalse(departments.isEmpty());
    }

    @Test
    void getUpdateKeysShouldReturn4() {
        JSONObject onboardingItem = new JSONObject("{\"Department_id_?7\":\"BlaBlaBla\", \"Test\":\"BlaBlaBla\", \"Department_id_8\":\"BlaBlaBla\", \"Department_id_?9\":\"BlaBlaBla\", \"Department_id_10\":\"BlaBlaBla\"}");
        ArrayList<String> departments = Endpoints.getUpdateKeys(onboardingItem);
        //If 4 departments are added with the correct format 'Department_id_x' and 1 random object only the correct departments should be added
        //This should return 4
        assertEquals(4, departments.size());
    }

    @Test
    void getUpdateKeysShouldThrowNullPointerException() {
        //If the update keys are requested with a null as the parameter for the item, it should throw a NullPointerException
        assertThrows(NullPointerException.class, () -> Endpoints.getUpdateKeys(null));
    }

    @Test
    void getDepartmentCountTestShouldReturn0() {
        String company = "{\"Domain_email\":\"@qwa.nl\",\"Company_id\":1,\"Company_name\":\"QWA\",\"Premium_account\":1,\"FK_Industry_id\":2}";
        //If a company is added without any departments no departments should be added.
        //The departmentCountTest function returns an int[] so we need to convert it to a stream and get the sum of all values, if all values are 0
        //The sum is also 0
        assertEquals(0, Arrays.stream(Endpoints.getDepartmentCountTest(company, 1)).sum());
    }

    @Test
    void getDepartmentCountTestShouldReturn2() {
        //If a company is added with 2 departments, 2 departments should be added.
        //The departmentCountTest function returns an int[] so we need to convert it to a stream and get the sum of all values, if all values are [1, 1]
        //The sum is also 2
        String company = "{\"Domain_email\":\"@qwa.nl\",\"Company_id\":1,\"Company_name\":\"QWA\",\"Premium_account\":1,\"FK_Industry_id\":2, \"Department_id_?7\":\"BlaBlaBla\", \"Department_id_8\":\"BlaBlaBla\",}";
        assertEquals(2, Arrays.stream(Endpoints.getDepartmentCountTest(company, 1)).sum());
    }

    @Test
    void getNewDepartmentCountShouldReturn1() {
        //If a company is added with 2 departments, 1 new and 1 needs to be edited, only 1 should be in the first index of the array.
        //The departmentCountTest function returns an int[] so we need to convert it to a stream and get the sum of all values, if all values are [1, 1]
        //The first index is the newDepartments and the second index is the need to be editedDepartments.
        //The first index in this test should be 1
        String company = "{\"Domain_email\":\"@qwa.nl\",\"Company_id\":1,\"Company_name\":\"QWA\",\"Premium_account\":1,\"FK_Industry_id\":2, \"Department_id_?7\":\"BlaBlaBla\", \"Department_id_8\":\"BlaBlaBla\",}";
        assertEquals(1, Endpoints.getDepartmentCountTest(company, 1)[0]);
    }

    @Test
    void getEditDepartmentCountShouldReturn2() {
        //If a company is added with 2 departments, 1 new and 1 needs to be edited, only 1 should be in the first index of the array.
        //The departmentCountTest function returns an int[] so we need to convert it to a stream and get the sum of all values, if all values are [1, 1]
        //The first index is the newDepartments and the second index is the need to be editedDepartments.
        //The second index in this test should be 2
        String company = "{\"Domain_email\":\"@qwa.nl\",\"Company_id\":1,\"Company_name\":\"QWA\",\"Premium_account\":1,\"FK_Industry_id\":2, \"Department_id_7\":\"BlaBlaBla\", \"Department_id_8\":\"BlaBlaBla\",}";
        assertEquals(2, Endpoints.getDepartmentCountTest(company, 1)[1]);
    }

    @Test
    void getEditDepartmentShouldThrowNullPointerException() {
        //If the department that need to be updated are requested with a null as the parameter for the item, it should throw a NullPointerException
        assertThrows(NullPointerException.class, () -> Endpoints.getDepartmentCountTest(null, 1));
    }
}