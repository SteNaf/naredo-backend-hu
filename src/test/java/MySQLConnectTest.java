import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MySQLConnectTest {

    @Test
    void propertiesShouldNotReturnNull() {
        MySQLConnect mySQLConnect = new MySQLConnect();
        //If properties is null the connection will fail, so it should not return null
        assertNotNull(mySQLConnect.getProperties());
    }

    @Test
    void connectionShouldNotReturnNull() {
        MySQLConnect mySQLConnect = new MySQLConnect();
        //If connection is null the connection has failed, so it should not return null
        assertNotNull(mySQLConnect.connect());
    }

    @Test
    void disconnectShouldReturnNull() {
        //Disconnect from database
        MySQLConnect.disconnect();
        assertNull(MySQLConnect.getConnection());
    }
}
